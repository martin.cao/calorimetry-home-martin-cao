from functions import m_json
from functions import m_pck

path = "/home/pi/calorimetry-home-martin-cao/datasheets/setup_newton.json"
dictionary = m_json.get_metadata_from_setup(path)
m_json.add_temperature_sensor_serials('home/pi/calorimetry-home-martin-cao/datasheets', dictionary)

messdata = m_pck.get_meas_data_calorimetry(dictionary)
m_pck.logging_calorimetry(messdata, dictionary, "/home/pi/calorimetry-home-martin-cao/data/newton", "/home/pi/calorimetry-home-martin-cao/datasheets")